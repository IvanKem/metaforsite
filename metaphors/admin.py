from django.contrib import admin


from .models import Metaphors


class MetaphorsAdmin(admin.ModelAdmin):
    list_display = ("name", "image",)


admin.site.register(Metaphors, MetaphorsAdmin)
# Register your models here.
