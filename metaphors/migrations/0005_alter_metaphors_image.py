# Generated by Django 3.2.5 on 2021-07-31 19:58

from django.db import migrations, models
import metaphors.models


class Migration(migrations.Migration):

    dependencies = [
        ('metaphors', '0004_alter_metaphors_image'),
    ]

    operations = [
        migrations.AlterField(
            model_name='metaphors',
            name='image',
            field=models.ImageField(upload_to=metaphors.models.hash_upload),
        ),
    ]
