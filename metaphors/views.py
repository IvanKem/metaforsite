
from django.db.models import Q
from django.db.models.functions import Lower
from django.http import JsonResponse
from django.shortcuts import render
from django.template.loader import render_to_string
from django.urls import reverse_lazy
from django.views.generic import TemplateView, ListView, CreateView

from .models import Metaphors


from django.core.mail import send_mail, BadHeaderError
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, redirect
from .forms import ContactForm
from MetaforSite.settings import EMAIL_HOST_USER, EMAIL_GETTER


class ImageCreateView(CreateView):
    model = Metaphors
    fields = ('name', 'image')
    template_name = 'add_image.html'
    success_url = reverse_lazy('home')


class ImageCreateViewEn(CreateView):
    model = Metaphors
    fields = ('en_name', 'image')
    template_name = 'add_image_en.html'
    success_url = reverse_lazy('home_en')


def page_not_found_view(request, exception):
    return render(request, '404.html', status=404)


def metaphors_get(request):
    ctx = {}
    query = request.GET.get("image_query", "")
    if query:
        object_list = Metaphors.objects.filter(Q(name__icontains=query))[:15]

    else:
        object_list = Metaphors.objects.all()[:15]

    ctx["object_list"] = object_list
    print(ctx)
    if request.is_ajax():
        html = render_to_string(
            template_name='home_ajax.html',
            context={'object_list': object_list})

        data_dict = {"html_from_view": html}
        return JsonResponse(data=data_dict, safe=False)

    if request.method == 'GET':
        form = ContactForm()
    elif request.method == 'POST':
        # если метод POST, проверим форму и отправим письмо
        form = ContactForm(request.POST)
        if form.is_valid():

            from_email = form.cleaned_data['from_email']
            message = form.cleaned_data['message']
            try:
                send_mail(f'Письмо с сайта метафор от {from_email}', message,
                          EMAIL_HOST_USER, [EMAIL_GETTER])
            except BadHeaderError:
                return HttpResponse('Ошибка в теме письма.')
            return redirect('/')
    else:
        return HttpResponse('Неверный запрос.')

    ctx['form'] = form

    return render(request, "home.html", context=ctx)


def metaphors_get_en(request):
    ctx_1 = {}
    query = request.GET.get("image_query", "")
    if query:
        object_list = Metaphors.objects.filter(Q(en_name__icontains=query))[:15]

    else:
        object_list = Metaphors.objects.all()[:15]

    ctx_1["object_list"] = object_list

    if request.is_ajax():
        html = render_to_string(
            template_name='home_ajax_en.html',
            context={'object_list': object_list})

        data_dict = {"html_from_view_1": html}
        return JsonResponse(data=data_dict, safe=False)

    if request.method == 'GET':
        form = ContactForm()
    elif request.method == 'POST':
        # если метод POST, проверим форму и отправим письмо
        form = ContactForm(request.POST)
        if form.is_valid():

            from_email = form.cleaned_data['from_email']
            message = form.cleaned_data['message']
            try:
                send_mail(f'Письмо с сайта метафор от {from_email}', message,
                          EMAIL_HOST_USER, [EMAIL_GETTER])
            except BadHeaderError:
                return HttpResponse('Ошибка в теме письма.')
            return redirect('/en')
    else:
        return HttpResponse('Неверный запрос.')

    ctx_1['form'] = form

    return render(request, "home_en.html", context=ctx_1)


