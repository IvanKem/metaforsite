from django.urls import path
from django.conf import settings
from django.conf.urls.static import static

from . import views
from .views import ImageCreateView, ImageCreateViewEn

urlpatterns = [

    path('add/', ImageCreateView.as_view(), name='add_image'),
    path('add/en/', ImageCreateViewEn.as_view(), name='add_image_en'),
    path('en/', views.metaphors_get_en, name='home_en'),
    path('', views.metaphors_get, name='home'),

]


urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)+static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)