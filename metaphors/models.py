import os
import hashlib

from deep_translator.exceptions import ElementNotFoundInGetRequest
from django.db import models
from deep_translator import GoogleTranslator


def hash_upload(instance, filename):
    instance.image.open()  # make sure we're at the beginning of the file
    contents = instance.image.read()  # get the contents
    fname, ext = os.path.splitext(filename)
    return "{0}/{1}{2}".format('image/', hashlib.sha256(contents).hexdigest(), ext)  # assemble the filename
    # raise Exception('файл уже есть')


# Create your models here.


class Metaphors(models.Model):
    name = models.CharField(max_length=250, default=None)
    en_name = models.CharField(max_length=250, default=None)
    image = models.ImageField(upload_to=hash_upload)

    def __str__(self):
        return self.name

    '''
    method to validate files if file exists 
    '''

    def save(self, *args, **kwargs):
        '''
        try:
            hash_upload()
        except Exception:
            return
        '''
        if self.name is None:
            try:
                self.name = GoogleTranslator(source='english', target='russian').translate(self.en_name)
                super(Metaphors, self).save(*args, **kwargs)
            except ElementNotFoundInGetRequest:
                self.name = ''
                super(Metaphors, self).save(*args, **kwargs)

        if self.en_name is None:
            try:
                self.en_name = GoogleTranslator(source='russian', target='english').translate(self.name)
                super(Metaphors, self).save(*args, **kwargs)
            except ElementNotFoundInGetRequest:
                self.en_name = ''
                super(Metaphors, self).save(*args, **kwargs)

        else:
            super(Metaphors, self).save(*args, **kwargs)