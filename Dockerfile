FROM python:3


WORKDIR /usr/src/app

COPY poetry.lock .
COPY pyproject.toml .
COPY entrypoint.sh .

RUN pip install poetry

RUN poetry export -f requirements.txt --output requirements.txt && pip install --no-cache-dir --upgrade -r requirements.txt



COPY . .

ENTRYPOINT ["/usr/src/app/entrypoint.sh"]